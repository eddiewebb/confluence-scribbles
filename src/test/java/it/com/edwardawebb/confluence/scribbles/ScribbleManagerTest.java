package it.com.edwardawebb.confluence.scribbles;

import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.atlassian.confluence.pages.Attachment;
import com.edwardawebb.confluence.scribbles.ImageAttachmentCreator;
import com.edwardawebb.confluence.scribbles.ScribbleSvg;


public class ScribbleManagerTest {

	
	private static final String SVG_DEF = "<svg width=\"32\" height=\"32\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns=\"http://www.w3.org/2000/svg\"><g><title>My Image</title><image id=\"svg_2\" width=\"32\" height=\"32\" y=\"0\" x=\"0\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3AkXFzgyr9cjLQAAAghJREFUWMPtlj1oFFEQx/9v9+42525hk0KwsvFAxCtEQYUgaRQOQRQsRJDzg0thoYUi4gciQkwEK+GEFDYBjxgDEkSbQETwLJLciaY4YoSgIiJiuD3f231vxiKLFhruCBsDsr9qhjfvzTDvDzNAQkJCwhoj2gWUGt4GHVIXKQGtCEYK6JBBCjAK0IqgpQCFHPmAkQSjBHQIVe0PPq64gFLD2wqgHgYhFn0FEwBaRkkUYCJby8iPbC0B17JBoQUBkX95K6ituEWlhlfpe7uepfaJO8SXi9RzzeWdFzIPYvmnU2/cqTP1jcTMRLx8HUTEzEz7B7ppzxWnGkvy07MuAKA47S1crG1v24Wj93LUc8N5BwC7LmViFKEUrg5oOu8V7GObbgtDGgDAtBRnizT6nxV5am7yh1G0zShhYhehVkAoAb+1JEAT+SYSXoZ+CzER4f8jQgA4MeOCSXg6MF/zXiF9dkvlr/fOjfTyzPyL76qlu23b1s8vB23ftjopYCjvw2hqgrC5+uWRqMxd/yNmcOIk1z5MCraQc9alOkrecQEAcH9HC6TF+xTSu4cbV1H9NPbr7OHrO3gyOyQs28qlUvg8cV6t7gQ78jRbLIyB57/V+dXCOO8tg/eVu3pXZRoux+HH2bu2dvqafhPQdmn8uCr/81l+cCQ7emDYubkmi8Sh0WyyTSUkxMJPIBAJhUKhN+8AAAAASUVORK5CYII\" /></g></svg>";
    private static final String BASE64_IMAGE = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3AkXFzgyr9cjLQAAAghJREFUWMPtlj1oFFEQx/9v9+42525hk0KwsvFAxCtEQYUgaRQOQRQsRJDzg0thoYUi4gciQkwEK+GEFDYBjxgDEkSbQETwLJLciaY4YoSgIiJiuD3f231vxiKLFhruCBsDsr9qhjfvzTDvDzNAQkJCwhoj2gWUGt4GHVIXKQGtCEYK6JBBCjAK0IqgpQCFHPmAkQSjBHQIVe0PPq64gFLD2wqgHgYhFn0FEwBaRkkUYCJby8iPbC0B17JBoQUBkX95K6ituEWlhlfpe7uepfaJO8SXi9RzzeWdFzIPYvmnU2/cqTP1jcTMRLx8HUTEzEz7B7ppzxWnGkvy07MuAKA47S1crG1v24Wj93LUc8N5BwC7LmViFKEUrg5oOu8V7GObbgtDGgDAtBRnizT6nxV5am7yh1G0zShhYhehVkAoAb+1JEAT+SYSXoZ+CzER4f8jQgA4MeOCSXg6MF/zXiF9dkvlr/fOjfTyzPyL76qlu23b1s8vB23ftjopYCjvw2hqgrC5+uWRqMxd/yNmcOIk1z5MCraQc9alOkrecQEAcH9HC6TF+xTSu4cbV1H9NPbr7OHrO3gyOyQs28qlUvg8cV6t7gQ78jRbLIyB57/V+dXCOO8tg/eVu3pXZRoux+HH2bu2dvqafhPQdmn8uCr/81l+cCQ7emDYubkmi8Sh0WyyTSUkxMJPIBAJhUKhN+8AAAAASUVORK5CYII";
	
    @Test
	public void svgXmlCanBeGeneratedFromAPngAttachment() throws IOException{
		Attachment attachment = mock(Attachment.class);
		when(attachment.getContentsAsStream()).thenReturn(getTestImageInputStream());
		when(attachment.getContentType()).thenReturn("image/png");
		when(attachment.getDisplayTitle()).thenReturn("My Image");
		when(attachment.getContentsAsStream()).thenReturn(getTestImageInputStream());
		String svg = ImageAttachmentCreator.svgDefinitionFromImageAttachment(attachment);
		//System.out.println(svg.getDefinition());
		//System.out.println(SVG_DEF);
		assertThat(svg,is(SVG_DEF));
		
	}
	
	
	@Test
	public void imageBinaryCanBeGeneratedFromBase64String() throws IOException{
		byte[] decodedImage = ImageAttachmentCreator.dataFromEncodedBase64PngString(BASE64_IMAGE);
		byte[] testImage = IOUtils.toByteArray(getTestImageInputStream());
		assertThat(decodedImage, is(testImage));
	}
	
	@Test
	// svg edit doesn't like trailing == in the uri, seems to be an issue for chrome to, so remove them
	public void anEncodedImagesTrailingEqualSignsAreRemoved(){
		String dirtyStringOne = "<element><image link=\"data:image/png;base64,hc87cd87c7nc=\"/></element>";
		String cleanStringOne = "<element><image link=\"data:image/png;base64,hc87cd87c7nc\"/></element>";
		String dirtyStringTwo = "<element><image link=\"data:image/png;base64,hc87cd87c7nc==\"/></element>";
		String cleanStringTwo = "<element><image link=\"data:image/png;base64,hc87cd87c7nc\"/></element>";
		
		ScribbleSvg scrib = new ScribbleSvg();
		
		scrib.setDefinition(dirtyStringOne);		
		assertThat(scrib.getDefinitionWithoutTrailingEqualSigns(),is(cleanStringOne));
		
		
		scrib.setDefinition(dirtyStringTwo);		
		assertThat(scrib.getDefinitionWithoutTrailingEqualSigns(),is(cleanStringTwo));
	}

	private InputStream getTestImageInputStream() throws FileNotFoundException {
		 InputStream is = this.getClass().getResourceAsStream("/images/2downarrow32.png");
		 return is;
	}
}
