/**
 * Copyright Oct 4, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.confluence.scribbles;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
//import org.slf4j.LoggerFactory;

/**
 * Trivial plugin creates the proper URL for the freemind-browser jar 
 * and attachment, along with default macro dimensions.
 * @author Eddie Webb
 */
public class ScribbleMacro implements Macro
{
	//private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FreemindMindmapMacro.class);



	/**
	 * 
	 * @param attachmentManager
	 * @param settingsManager
	 */
    public ScribbleMacro( AttachmentManager attachmentManager,SettingsManager settingsManager)
    {
        
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException
    {
    	
    	return "hi";
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }


    
}
