/**
 * Copyright 2013 - Edward A. Webb
 *
 * This file is par of Confluence Scribbles
 * HTML5 based editor to insert scribbles and drawings in Confluence pages
 * 
 * Confluence Scribbles is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 * Confluence Scribbles is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Confluence Scribbles.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.edwardawebb.confluence.scribbles;

import static com.edwardawebb.confluence.scribbles.ImageAttachmentCreator.svgDefinitionFromImageAttachment;

import java.io.IOException;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;

/**
 * Responsible for loading attachments from pages and returning the SVG
 * counterpart if available, or creating the initial SVG for rasters.
 * 
 * @author Edward A. Webb (http://edwardawebb.com)
 * 
 */
public class ScribbleServiceBandanaImpl implements ScribbleService {
	private static final BandanaContext BANDANA_ATTACHMENT_CONTEXT=new ConfluenceBandanaContext();
	
	private static Logger LOG = LoggerFactory
			.getLogger(ScribbleServiceBandanaImpl.class);

	private final AttachmentManager attachmentManager;
	private final BandanaManager bandanaManager;
	 

	public ScribbleServiceBandanaImpl(AttachmentManager attachmentManager,
			BandanaManager bandanaManager) {
		this.attachmentManager = attachmentManager;
		this.bandanaManager = bandanaManager;
	}

	/* (non-Javadoc)
	 * @see com.edwardawebb.confluence.scribbles.ScribbleService#loadOrCreateSvgDefinitionFor(com.atlassian.confluence.core.ContentEntityObject, java.lang.String)
	 */
	@Override
	public ScribbleSvg loadOrCreateSvgDefinitionForAttachment(ContentEntityObject page,
			String attachmentName) throws IOException {
		ScribbleSvg scribbleSvg = null;
		Attachment attachment = attachmentManager.getAttachment(page,
				attachmentName);
		if (null != attachment) {
			try {
				scribbleSvg = loadExistingSvgForAttachment(attachment);
			} catch (ScribbleDoesNotExistException svde) {
				scribbleSvg = newPersistedSvgDefinition(attachment);
			}
		} else {
			throw new IllegalArgumentException("No attachment with the name " + attachmentName + " exists for page ID " + page.getId() + ". A backing ScribbleSvg cannot be created. Create a new one instead");
		}
		return scribbleSvg;

	}

	/* (non-Javadoc)
	 * @see com.edwardawebb.confluence.scribbles.ScribbleService#saveSvgDefinition(com.edwardawebb.confluence.scribbles.ScribbleSvg, com.atlassian.confluence.pages.Attachment)
	 */
	@Override
	public void updateSvgDefinition(String svgDefinition, Attachment attachment) throws IOException, ScribbleDoesNotExistException {
		
		ScribbleSvg scribbleSvg = findScribbleByAttachment(attachment);		
		if(null == scribbleSvg){
			LOG.warn("NO scribble exists for this attachment yet");
			throw new ScribbleDoesNotExistException();
		}
		if (scribbleSvg.getAttachmentVersion() != 0
				&& scribbleSvg.getAttachmentVersion() == attachment
						.getVersion()) {
			throw new IllegalStateException(
					"New Attachment must be persisted prior to updating ScribbleSvg. " +
					"The Attachment version already matches the version known to this scribble, " +
					"Please persist the attachment as a new version before calling. ");
		}
		scribbleSvg.setAttachmentId(attachment.getId());
		scribbleSvg.setAttachmentVersion(attachment.getVersion());
		scribbleSvg.setDefinition(svgDefinition);
		saveScribble(scribbleSvg);
	}
	
	/* (non-Javadoc)
	 * @see com.edwardawebb.confluence.scribbles.ScribbleService#saveSvgDefinition(com.edwardawebb.confluence.scribbles.ScribbleSvg, com.atlassian.confluence.pages.Attachment)
	 */
	@Override
	public void saveNewDefinition(String svgDefinition, Attachment attachment) throws IOException {
		
		ScribbleSvg scribbleSvg = new ScribbleSvg();		
		scribbleSvg.setAttachmentId(attachment.getId());
		scribbleSvg.setAttachmentVersion(attachment.getVersion());
		scribbleSvg.setDefinition(svgDefinition);
		saveScribble(scribbleSvg);
	}


	/**
	 * this should ONLY be called by lookup operations. Using to persiste will delete previous and throw exception
	 * @param attachment
	 * @return
	 * @throws ScribbleDoesNotExistException
	 */
	private ScribbleSvg loadExistingSvgForAttachment(Attachment attachment) throws ScribbleDoesNotExistException {
		ScribbleSvg scribbleSvg = findScribbleByAttachment(attachment);		
		if(null != scribbleSvg){
			if(scribbleSvg.getAttachmentVersion() != attachment.getVersion()){
				LOG.warn("Found scribble for attachment: {} with older version. ",attachment.getId());
				LOG.warn("Expired scribble will be deleted, and a new one created from most current version");
				deleteScribble(scribbleSvg);				
			}else{
				LOG.info("Returning matching Scribble for attachment{}",attachment.getId());
				return scribbleSvg;
			}
		} 
		LOG.info("No ScribbleSvg could be found for Attachment {}, a new one will be created",attachment.getId());	
		
		throw new ScribbleDoesNotExistException();
	}

		

	private ScribbleSvg newPersistedSvgDefinition(Attachment attachment) throws IOException {
		ScribbleSvg scribbleSvg =  new ScribbleSvg();
		scribbleSvg.setDefinition(svgDefinitionFromImageAttachment(attachment));
		scribbleSvg.setAttachmentId(attachment.getId());
		scribbleSvg.setAttachmentVersion(attachment.getVersion());
		saveScribble(scribbleSvg);
		return scribbleSvg;

	}
	
	private ScribbleSvg findScribbleByAttachment(Attachment attachment){
		return (ScribbleSvg) bandanaManager.getValue(BANDANA_ATTACHMENT_CONTEXT, ""+attachment.getId());		
	}

	private void saveScribble(ScribbleSvg scribbleSvg) {
		Validate.notNull(scribbleSvg.getAttachmentId());
		bandanaManager.setValue(BANDANA_ATTACHMENT_CONTEXT, ""+scribbleSvg.getAttachmentId(),scribbleSvg);
	}
	

	private void deleteScribble(ScribbleSvg scribbleSvg) {
		Validate.notNull(scribbleSvg.getAttachmentId());
		bandanaManager.removeValue(BANDANA_ATTACHMENT_CONTEXT, ""+scribbleSvg.getAttachmentId());
	
	}
	

}
