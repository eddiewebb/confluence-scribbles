package com.edwardawebb.confluence.scribbles;

import java.io.IOException;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;

public interface ScribbleService {

	/**
	 * Given an existing attachment this will load the ScribbleSvg if present, 
	 * or create anew one with the attachment embedded
	 * @param page	 
	 * @param attachmentName
	 * @return
	 * @throws IOException
	 */
	ScribbleSvg loadOrCreateSvgDefinitionForAttachment(
			ContentEntityObject page, String attachmentName) throws IOException;

	/**
	 * provide updated SVG definition for an existing {@link ScribbleSvg} and persist
	 * 
	 * 
	 * @param imageSvg
	 * @param attachment
	 * @throws IOException 
	 * @throws ScribbleDoesNotExistException 
	 */
	void updateSvgDefinition(String imageSvg,
			Attachment attachment) throws IOException, ScribbleDoesNotExistException;

	void saveNewDefinition(String svgDefinition, Attachment attachment)
			throws IOException;


}