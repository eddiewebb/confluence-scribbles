/**
 * Copyright 2013 - Edward A. Webb
 *
 * This file is par of Confluence Scribbles
 * HTML5 based editor to insert scribbles and drawings in Confluence pages
 * 
 * Confluence Scribbles is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 * Confluence Scribbles is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Confluence Scribbles.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.edwardawebb.confluence.scribbles.actions;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.actions.PageAware;
import com.edwardawebb.confluence.scribbles.ImageAttachmentCreator;
import com.edwardawebb.confluence.scribbles.ScribbleSvg;
import com.edwardawebb.confluence.scribbles.ScribbleService;

/**
 * @author Edward A. Webb (http://edwardawebb.com)
 *
 */
public class ScribbleEditActionSvg extends ConfluenceActionSupport implements PageAware {
	private static Logger LOG = LoggerFactory.getLogger(ScribbleEditActionSvg.class);
	
	private AbstractPage page;
	private String attachmentName ;
	private String svgDefinition;
	private boolean existing =false;
	private ScribbleService scribbleService;
	private ScribbleSvg scribbleSvg;
	
	
	
	
	public ScribbleEditActionSvg(ScribbleService scribbleService) {
		super();
		this.scribbleService = scribbleService;
	}

	@Override
	public String execute() throws Exception {
		loadSvgDefinition();
		return SUCCESS;
      }

	private void loadSvgDefinition() throws IOException {
		if(!StringUtils.isEmpty(attachmentName)){
			svgDefinition = scribbleService.loadOrCreateSvgDefinitionForAttachment(page,attachmentName).getDefinitionWithoutTrailingEqualSigns();
			existing = true;
			LOG.debug("loaded ScribbleSvg {}",scribbleSvg);
		}else{
			svgDefinition = ImageAttachmentCreator.newEmptyDefinition();
		}
		
	}

	/**
     * Implementation of PageAware
     */
    public AbstractPage getPage()
    {
        return page;
    }
 
    /**
     * Implementation of PageAware
     */
    public void setPage(AbstractPage page)
    {
        this.page = page;
    }
 
    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the
     * page is set before the action commences.
     */
    public boolean isPageRequired()
    {
        return true;
    }
 
    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the
     * current version of the page is used.
     */
    public boolean isLatestVersionRequired()
    {
        return true;
    }
 
    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the user
     * requires page view permissions.
     */
    public boolean isViewPermissionRequired()
    {
        return true;
    }


	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}


	public boolean isExisting() {
		return existing;
	}

	public void setExisting(boolean isExisting) {
		this.existing = isExisting;
	}

	public String getSvgDefinition() {
		//strip new lines
		return svgDefinition.replaceAll("\n", "").replaceAll("\r", "");
	}

	public void setSvgDefinition(String svgDefintion) {
		this.svgDefinition = svgDefintion;
	}

	public ScribbleSvg getScribble() {
		return scribbleSvg;
	}

	public void setScribble(ScribbleSvg scribbleSvg) {
		this.scribbleSvg = scribbleSvg;
	}
 

	
	
}
