package com.edwardawebb.confluence.scribbles;



import com.atlassian.confluence.pages.Attachment;

/**
 * Represents the complete graphic of a scribbles and the image {@link Attachment} it is linked to.
 * Scribbles are never rendered on screen directly in display mode, but instead exported to a new or existing {@link Attachment} when saved.
 *  When an existing {@link Attachment} is edited, the backing ScribbleSvg will be created or retrieved.
 *  Scribbles will only be retrieved if it matches both the ID and the version of the {@link Attachment} being edited.
 *  Otherwise a new ScribbleSvg will be created for that {@link Attachment}
 * @author Edward A. Webb (http://edwardawebb.com)
 *
 */
public class ScribbleSvg {

	private long attachmentId;
	private int attachmentVersion;
	private String definition;
	
	
	
	public long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(long attachmentId) {
		this.attachmentId = attachmentId;
	}
	public int getAttachmentVersion() {
		return attachmentVersion;
	}
	public void setAttachmentVersion(int attachmentVersion) {
		this.attachmentVersion = attachmentVersion;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	public String getDefinition() {
		return definition;
	}
	public String getDefinitionWithoutTrailingEqualSigns() {
		//match entire image string, leave out ='s in replacement
		return definition.replaceAll("(data:image/png;base64,[A-Za-z0-9+/]*)={1,2}", "$1");
	}

	

}